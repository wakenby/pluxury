export function importAll () {
  const initModules = req => req.keys().forEach(key => Object.values(req(key))[0]())

  const importPages = require.context('@pages', true, /\.js$/)
  const importComponents = require.context('@components', true, /\.js$/)

  initModules(importPages)
  initModules(importComponents)
}
