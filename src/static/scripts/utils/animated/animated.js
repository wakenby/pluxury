import './animated.scss'
import { scroll } from '@helpers/scroll'
import { isElementInViewport } from '@helpers/is-element-in-viewport'

export function animated () {
  const fadeUps = document.querySelectorAll('[fade-up]')
  let elements = [...fadeUps]

  const reset = (element) => {
    element.removeAttribute('fade-up')
  }

  const check = (element) => {
    if (isElementInViewport(element)) {
      reset(element)
      elements = elements.filter(item => item !== element)
    }
  }

  scroll((lastScrollY, callback) => {
    elements.forEach(element => setTimeout(() => check(element), 16))

    if (elements.length) callback()
  })
}
