export async function appendSprite () {
  const response = await fetch('/assets/sprite/sprite.svg')
  const svg = await response.text()

  if (response.ok) prepend(svg)
  else console.error('sprite.svg not found :(')

  function prepend (sprite) {
    const hiddenBox = document.createElement('div')

    hiddenBox.style.cssText = 'height: 0; width: 0; position: absolute; pointer-events: none;'
    hiddenBox.insertAdjacentHTML('afterbegin', sprite)

    document.body.prepend(hiddenBox)
  }
}
