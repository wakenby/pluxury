export function scroll (handleUpdate) {
  const loop = () => {
    handleUpdate(window.pageYOffset, () => {
      window.requestAnimationFrame(loop)
    })
  }

  loop()
}
