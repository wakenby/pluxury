export function isElementInViewport (el) {
  const rect = el.getBoundingClientRect()

  const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
  const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)

  const vertInView = rect.top <= vh && rect.top + rect.height >= 0
  const horInView = rect.left <= vw && rect.left + rect.width >= 0

  return vertInView && horInView
}
