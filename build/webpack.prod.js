const dev = require('./webpack.dev')
const { merge } = require('webpack-merge')
const imageMinimizerWebpackPlugin = require('./slices/plugins/image-minimizer-webpack-plugin')
const terserWebpackPlugin = require('./slices/plugins/terser-webpack-plugin')
const optimizeCssAssetsWebpackPlugin = require('./slices/plugins/optimize-css-assets-webpack-plugin')

module.exports = merge(dev, {
  devtool: false,
  optimization: {
    minimize: true,
    minimizer: [
      ...terserWebpackPlugin,
      ...optimizeCssAssetsWebpackPlugin
    ]
  },
  plugins: [
    ...imageMinimizerWebpackPlugin
  ]
})
