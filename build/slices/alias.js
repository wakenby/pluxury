const cwd = require('./cwd')

module.exports = {
  '@components': cwd('src/components'),
  '@pages': cwd('src/pages'),
  '@helpers': cwd('src/static/scripts/helpers'),
  '@utils': cwd('src/static/scripts/utils'),
  '@static': cwd('src/static')
}
