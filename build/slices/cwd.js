const path = require('path')
const cwd = (string) => path.resolve(process.cwd(), string)

module.exports = cwd
