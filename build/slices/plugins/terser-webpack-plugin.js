const TerserWebpackPlugin = require('terser-webpack-plugin')

module.exports = [
  new TerserWebpackPlugin({
    terserOptions: {
      output: {
        comments: false
      }
    },
    extractComments: false
  })
]
