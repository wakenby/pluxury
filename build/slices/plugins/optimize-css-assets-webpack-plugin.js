const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = [
  new OptimizeCssAssetsWebpackPlugin()
]
