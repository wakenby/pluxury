const SvgSpriteMapWebpackPlugin = require('svg-spritemap-webpack-plugin')
const cwd = require('../cwd')

module.exports = [
  new SvgSpriteMapWebpackPlugin(cwd('src/static/icons/*.svg'),
    {
      output: {
        filename: 'assets/sprite/sprite.svg',
        svgo: true
      },
      sprite: {
        prefix: false,
        generate: {
          title: false
        }
      }
    }
  )
]
