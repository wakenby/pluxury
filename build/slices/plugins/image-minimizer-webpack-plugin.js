const Imagemin = require('image-minimizer-webpack-plugin')

module.exports = [
  new Imagemin({
    test: /\.(jpeg|jpg|png)$/i,
    filename: '[path][name].webp',
    minimizerOptions: {
      plugins: [
        ['imagemin-webp', { quality: 100 }]
      ]
    }
  }),
  new Imagemin({
    test: /\.(jpeg|jpg|png)$/i,
    minimizerOptions: {
      plugins: [
        ['imagemin-mozjpeg', { progressive: true, quality: 100 }],
        ['imagemin-optipng', { optimizationLevel: 3 }]
      ]
    }
  })
]
