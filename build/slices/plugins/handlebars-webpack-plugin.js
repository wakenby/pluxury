const HandlebarsWebpackPlugin = require('handlebars-webpack-plugin')
const { isProd } = require('../env')
const cwd = require('../cwd')

function addTag (resultHtml) {
  const headRegExp = /(<\/head\s*>)/i
  const bodyRegExp = /(<\/body\s*>)/i

  const linkPath = 'css/styles.css'
  const scriptPath = 'js/main.js'

  const link = `<link rel="stylesheet" href="${linkPath}">`
  const script = `<script src="${scriptPath}"></script>`

  if (bodyRegExp.test(resultHtml)) resultHtml = resultHtml.replace(bodyRegExp, match => script + match)
  else resultHtml = resultHtml + script

  if (headRegExp.test(resultHtml)) resultHtml = resultHtml.replace(headRegExp, match => link + match)
  else resultHtml = link + resultHtml

  return resultHtml
}

function replaceImgToPicture (resultHtml) {
  return resultHtml.replace(/(?!.* not-replace)(?!.* src="https?:\/\/)(<img .*?src="(.*?)\..*?".*?>)/g, `
    <picture>
      <source srcset="$2.webp" type="image/webp">
      $1
    </picture>
  `)
}

module.exports = [
  new HandlebarsWebpackPlugin({
    entry: cwd('src/pages/*/[!_]*.hbs'),
    output: cwd('dist/[name].html'),
    partials: [
      cwd('src/components/*/*.hbs'),
      cwd('src/pages/*/[_]*.hbs')
    ],
    getPartialId: filePath => filePath.match(/\/([^/]+)\.[^.]+$/).pop(),
    onBeforeSave: (Handlebars, resultHtml, filename) => {
      resultHtml = addTag(resultHtml)
      resultHtml = isProd ? replaceImgToPicture(resultHtml) : resultHtml

      return resultHtml
    }
  })
]
