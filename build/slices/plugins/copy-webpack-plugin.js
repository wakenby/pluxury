const CopyWebpackPlugin = require('copy-webpack-plugin')
const cwd = require('../cwd')

module.exports = [
  new CopyWebpackPlugin({
    patterns: [
      { from: cwd('src/assets'), to: 'assets' },
      { from: cwd('src/static/misc'), to: '' }
    ]
  })
]
