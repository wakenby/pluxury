const cssExtractLoader = require('mini-css-extract-plugin').loader
const autoprefixer = require('autoprefixer')
const globImporter = require('node-sass-glob-importer')
const minmaxMedia = require('postcss-media-minmax')
const webpCssClass = require('webp-in-css/plugin')
const cwd = require('../cwd')
const { isProd } = require('../env')

module.exports = {
  test: /\.scss$/,
  use: [
    cssExtractLoader,
    {
      loader: 'css-loader',
      options: {
        url: false
      }
    },
    {
      loader: 'postcss-loader',
      options: {
        postcssOptions: {
          plugins: [
            autoprefixer(),
            minmaxMedia(),
            isProd ? webpCssClass() : ''
          ]
        }
      }
    },
    {
      loader: 'sass-loader',
      options: {
        sassOptions: {
          importer: globImporter(),
          includePaths: [
            cwd('src')
          ]
        }
      }
    }
  ]
}
