const { merge } = require('webpack-merge')
const dev = require('./webpack.dev')
const cwd = require('./slices/cwd')

module.exports = merge(dev, {
  devServer: {
    injectClient: true,
    contentBase: cwd('dist'),
    watchContentBase: true,
    port: 8080,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    }
  }
})
