const cwd = require('./slices/cwd')
const jsLoader = require('./slices/rules/js')
const scssLoader = require('./slices/rules/scss')
const alias = require('./slices/alias')
const copyWebpackPlugin = require('./slices/plugins/copy-webpack-plugin')
const handlebarsWebpackPlugin = require('./slices/plugins/handlebars-webpack-plugin')
const miniCssExtractPlugin = require('./slices/plugins/mini-css-extract-plugin')
const cleanWebpackPlugin = require('./slices/plugins/clean-webpack-plugin')
const svgSpriteMapWebpackPlugin = require('./slices/plugins/svg-spritemap-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: [
    cwd('src/static/styles/styles.scss'),
    cwd('src/static/scripts/main.js')
  ],
  output: {
    path: cwd('dist'),
    filename: 'js/main.js'
  },
  module: {
    rules: [
      jsLoader,
      scssLoader
    ]
  },
  resolve: {
    alias
  },
  devtool: 'source-map',
  target: 'web',
  optimization: {
    noEmitOnErrors: true
  },
  plugins: [
    ...cleanWebpackPlugin,
    ...handlebarsWebpackPlugin,
    ...miniCssExtractPlugin,
    ...copyWebpackPlugin,
    ...svgSpriteMapWebpackPlugin
  ]
}
